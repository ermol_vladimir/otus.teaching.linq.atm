﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            System.Console.WriteLine("\r\n Task1");
            var user = PrintUserInfo(atmManager, "snow", "111");
            System.Console.WriteLine("\r\n Task2");
            PrintUserAccounts(atmManager, user);
            System.Console.WriteLine("\r\n Task3");
            PrintUserAccountsWithHistory(atmManager, user);
            System.Console.WriteLine("\r\n Task4");
            PrintInputOperations(atmManager);
            System.Console.WriteLine("\r\n Task5");
            PrintRichUsers(atmManager, 10);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }

        static Core.Entities.User PrintUserInfo(ATMManager atmManager, string login, string pass)
        {
            var userInfo = atmManager.Users.Where(u => (u.Login == login) && (u.Password == pass)).FirstOrDefault();
            if (userInfo != null)
            {
                System.Console.WriteLine($"Найден пользователь {userInfo.Id}: {userInfo.SurName}  {userInfo.FirstName}");
            }
            else
            {
                System.Console.WriteLine("Указанного пользователя не существует");
            }
            return userInfo;
        }

        static void PrintUserAccounts(ATMManager atmManager, Core.Entities.User user)
        {
            if (user == null)
            {
                System.Console.WriteLine("Не задан пользователь для поиска счетов");
                return;
            }

            var userAccounts = atmManager.Accounts.Where(a => a.UserId == user.Id).ToList();

            System.Console.WriteLine($"У пользователя {user.Id} {user.SurName} {user.FirstName} найдено {userAccounts.Count} счетов:");
            userAccounts.ForEach(a =>
            {
                System.Console.WriteLine($"Счет {a.Id}, остаток: {a.CashAll}");
            });
        }

        static void PrintUserAccountsWithHistory(ATMManager atmManager, Core.Entities.User user)
        {
            if (user == null)
            {
                System.Console.WriteLine("Не задан пользователь для поиска счетов");
                return;
            }

            //var userAccounts = (
            //from account in atmManager.Accounts
            //where account.UserId == user.Id
            //select new
            //{
            //    Account = account,
            //    AccountHistory = (
            //    from operation in atmManager.History
            //    where operation.AccountId == account.Id
            //    select new { operation }).ToList()
            //}).ToList();

            //System.Console.WriteLine($"У пользователя {user.Id} {user.SurName} {user.FirstName} найдено {userAccounts.Count} счетов:");

            //userAccounts.ForEach(a =>
            //{
            //    System.Console.WriteLine($"Счет {a.Account.Id}, остаток {a.Account.CashAll}, история операций:");

            //    a.AccountHistory.ForEach(h =>
            //    {
            //        System.Console.WriteLine($"Операция {h.operation.Id}, {h.operation.OperationDate}, {h.operation.OperationType}, {h.operation.CashSum}");
            //    });
            //});

            var userAccounts = (
            from account in atmManager.Accounts
            where account.UserId == user.Id
            join operation in atmManager.History on account.Id equals operation.AccountId
            select new
            {
                AccountId = account.Id,
                AccountCashAll = account.CashAll,
                OperationId = operation.Id,
                OperationDate = operation.OperationDate,
                OperationType = operation.OperationType,
                OperationCashSum = operation.CashSum
            }).ToList();

            int accountId = -1; //допущение - ID счета не может быть отрицательным

            userAccounts.ForEach(a =>
            {
                if (accountId != a.AccountId)
                {
                    accountId = a.AccountId;
                    System.Console.WriteLine($"Счет {a.AccountId}, остаток {a.AccountCashAll}, история операций:");
                }
                
                System.Console.WriteLine($"Операция {a.OperationId}, {a.OperationDate}, {a.OperationType}, {a.OperationCashSum}");
            });
        }

        static void PrintInputOperations(ATMManager atmManager)
        {
            var operations = (
                from operation in atmManager.History
                where operation.OperationType == OperationType.InputCash
                join account in atmManager.Accounts on operation.AccountId  equals account.Id
                join user in atmManager.Users on account.UserId equals user.Id
                select new { Operation = operation, UserFirstName = user.FirstName, UserSurName = user.SurName }).ToList();

            operations.ForEach(a =>
            {
                System.Console.WriteLine($"Операция {a.Operation.Id} пополнения на {a.Operation.CashSum}, влыделец счета {a.UserSurName} {a.UserFirstName}");
            });
        }

        static void PrintRichUsers(ATMManager atmManager, decimal minCash)
        {
            var users = (
                from account in atmManager.Accounts
                where account.CashAll > minCash
                join user in atmManager.Users on account.UserId equals user.Id
                select user).ToHashSet();

            if (users.Count > 0)
            {
                System.Console.WriteLine($"Богачи:");
                foreach (var u in users)
                {
                    System.Console.WriteLine($"{u.SurName} {u.FirstName}");
                }
            }
            else
            {
                System.Console.WriteLine($"Таких богатых у нас нет");
            }
        }
    }
}